﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MSI.GsmCommunication;

namespace SMSLibrary
{
   public class GsmComSetting
    {





        public static List<SMSContents> gsmMessages;
        public static List<SMSContents> SelectMessageContact(string smsText, string contact)
        {
            gsmMessages = new List<SMSContents>();

            if (contact.Trim().Length >=11 && smsText.Length > 0)
            {
                gsmMessages.Add(new SMSContents() { reciverNumber = contact, smsText = smsText.Trim() });
            }
            return gsmMessages;
        }





        public static GsmCommMain DefaultInfo(int PortNo, int batrate, int timeouts)
        {

            int protNo;
            int baudRate;
            int timeout;
            ManageSmsGsm mng;
            GsmSMS gsmms = new GsmSMS(); gsmms = null;
            GsmCommMain gsmcomMain = new GsmCommMain(); gsmcomMain = null;
            if (PortNo > 0 && batrate > 0 && timeouts > 0)
            {
                protNo = PortNo;
                baudRate = batrate;
                timeout = timeouts;
                gsmms = new GsmSMS(protNo, baudRate, timeout);

                mng = new ManageSmsGsm();
                gsmcomMain = mng.SetGsmconnection(gsmms);
            }
            else
            {
                return gsmcomMain;
            }


            return gsmcomMain;
        }
     
    }
}
