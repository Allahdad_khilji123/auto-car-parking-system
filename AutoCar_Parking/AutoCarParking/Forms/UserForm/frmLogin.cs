﻿using AutoCarParking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.UserForm
{
    public partial class frmLogin : Form
    {
        private HomeForm FormHome;
        public frmLogin(HomeForm homeForm)
        {
            InitializeComponent();
            FormHome = homeForm;
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            ep.Clear();
            if (txtUserName.Text.Trim().Length == 0)
            {
                ep.SetError(txtUserName, "Please Enter User Name!");
                txtUserName.Focus();
                return;
            }

            if (txtPassword.Text.Trim().Length == 0)
            {
                ep.SetError(txtPassword, "Please Enter Password!");
                txtPassword.Focus();
                return;
            }

            string loginquery = "select * from UserTable where UserName = '" + txtUserName.Text.Trim() + "' AND Password = '" + txtPassword.Text.Trim() + "'";
            DataTable dt = DatabaseAccess.Select(loginquery);
            if (dt != null)
            {
                if (dt.Rows.Count == 1)
                {
                    FormHome.hometoolsstrip.Enabled = true;
                    FormHome.btnLoginuser.Visible = false;
                    FormHome.btnLogin.Visible = true;
                    this.Hide();
                }
            }
        }
    }
}
