﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auto_parking.SourceCode
{        
    public class PortConfiguration
    {
        public static int portNo;
        public static int bautRate;
        public static int Timeout;

        public void setPortNo(int setport)
        {
            portNo = setport;
        }
        public void setbautRate(int bautrate)
        {
            bautRate = bautrate;
        }
        public void setTimeout(int timeout)
        {
            Timeout = timeout;
        }


    }

    public class GSMConfiguration
    {
        public static string SendSMS5MinTest(string smsText, string contactList, int COMNO, int bautRate, int Timeout)
        {
            bool reuslt;
            try
            {
                SMSLibrary.ManageSmsGsm mngGsm = new SMSLibrary.ManageSmsGsm();
                reuslt = mngGsm.SendSMSQueu(SMSLibrary.GsmComSetting.DefaultInfo(COMNO, bautRate, Timeout), SMSLibrary.GsmComSetting.SelectMessageContact(smsText, contactList));
                if (reuslt == true)
                    return "sucess";
                // else if (reuslt == "Message service error 313 occurred.")
                // return "Message service error 313 occurred.";
                else
                    return "sending Failed";

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
    }
}
