﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.SourceCode
{          
   public class CombList
    {
        public static void FillDriverTypes(ComboBox cmb)
        {
            DataTable dtDriverType = new DataTable();
            dtDriverType.Columns.Add("DriverTypeID");
            dtDriverType.Columns.Add("DriverType");
            dtDriverType.Rows.Add("0", "---Select---");
            try
            {
                DataTable dt = DatabaseAccess.Select("select DriverTypeID, DriverType from DriverTypeTable");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow usertype in dt.Rows)
                        {
                            dtDriverType.Rows.Add(usertype["DriverTypeID"], usertype["DriverType"]);
                        }
                    }

                }
                cmb.DataSource = dtDriverType;
                cmb.ValueMember = "DriverTypeID";
                cmb.DisplayMember = "DriverType";
            }
            catch
            {
                cmb.DataSource = dtDriverType;
                cmb.ValueMember = "DriverTypeID";
                cmb.DisplayMember = "DriverType";
            }
        }

        public static void FillAutoTypes(ComboBox cmb)
        {
            DataTable dtCarType = new DataTable();
            dtCarType.Columns.Add("CarTypeID");
            dtCarType.Columns.Add("CarType");
            dtCarType.Rows.Add("0", "---Select---");
            try
            {
                DataTable dt = DatabaseAccess.Select("select CarTypeID, CarType from CarTypeTable");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow usertype in dt.Rows)
                        {
                            dtCarType.Rows.Add(usertype["CarTypeID"], usertype["CarType"]);
                        }
                    }

                }
                cmb.DataSource = dtCarType;
                cmb.ValueMember = "CarTypeID";
                cmb.DisplayMember = "CarType";
            }
            catch
            {
                cmb.DataSource = dtCarType;
                cmb.ValueMember = "CarTypeID";
                cmb.DisplayMember = "CarType";
            }
        }

    }
}
