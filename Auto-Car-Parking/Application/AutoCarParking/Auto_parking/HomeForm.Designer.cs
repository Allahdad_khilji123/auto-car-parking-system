﻿namespace Auto_parking
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            this.hometoolsstrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCarTypes = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnPersonRegistration = new System.Windows.Forms.ToolStripButton();
            this.tsbtnAllVehicle = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnCheckin = new System.Windows.Forms.ToolStripButton();
            this.tsbtnAutoCheckin = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCheckout = new System.Windows.Forms.ToolStripButton();
            this.tsbtnParkingHistory = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.panelParent = new System.Windows.Forms.Panel();
            this.btnLoginuser = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.hometoolsstrip.SuspendLayout();
            this.panelParent.SuspendLayout();
            this.SuspendLayout();
            // 
            // hometoolsstrip
            // 
            this.hometoolsstrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.hometoolsstrip.Enabled = false;
            this.hometoolsstrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.hometoolsstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.tsbtnCarTypes,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.tsbtnPersonRegistration,
            this.tsbtnAllVehicle,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.tsbtnCheckin,
            this.tsbtnAutoCheckin,
            this.tsbtnCheckout,
            this.tsbtnParkingHistory,
            this.toolStripLabel3,
            this.toolStripButton2});
            this.hometoolsstrip.Location = new System.Drawing.Point(0, 0);
            this.hometoolsstrip.Name = "hometoolsstrip";
            this.hometoolsstrip.Size = new System.Drawing.Size(114, 633);
            this.hometoolsstrip.TabIndex = 0;
            this.hometoolsstrip.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::Auto_parking.Properties.Resources.drivericon;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(111, 51);
            this.toolStripButton1.Text = "Driver Types";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.ToolStripButton1_Click);
            // 
            // tsbtnCarTypes
            // 
            this.tsbtnCarTypes.Image = global::Auto_parking.Properties.Resources.cartypesicon;
            this.tsbtnCarTypes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCarTypes.Name = "tsbtnCarTypes";
            this.tsbtnCarTypes.Size = new System.Drawing.Size(111, 51);
            this.tsbtnCarTypes.Text = "Auto Types";
            this.tsbtnCarTypes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnCarTypes.Click += new System.EventHandler(this.TsbtnCarTypes_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(111, 6);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 10);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(101, 21);
            this.toolStripLabel1.Text = "Registration";
            // 
            // tsbtnPersonRegistration
            // 
            this.tsbtnPersonRegistration.Image = global::Auto_parking.Properties.Resources.drivericon1;
            this.tsbtnPersonRegistration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnPersonRegistration.Name = "tsbtnPersonRegistration";
            this.tsbtnPersonRegistration.Size = new System.Drawing.Size(111, 51);
            this.tsbtnPersonRegistration.Text = "Registration";
            this.tsbtnPersonRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnPersonRegistration.Click += new System.EventHandler(this.TsbtnPersonRegistration_Click);
            // 
            // tsbtnAllVehicle
            // 
            this.tsbtnAllVehicle.Image = global::Auto_parking.Properties.Resources.cartypesicon1;
            this.tsbtnAllVehicle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAllVehicle.Name = "tsbtnAllVehicle";
            this.tsbtnAllVehicle.Size = new System.Drawing.Size(111, 51);
            this.tsbtnAllVehicle.Text = "All Vehicles";
            this.tsbtnAllVehicle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnAllVehicle.Click += new System.EventHandler(this.TsbtnAllVehicle_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(111, 6);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 10);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(101, 21);
            this.toolStripLabel2.Text = "Parking";
            // 
            // tsbtnCheckin
            // 
            this.tsbtnCheckin.Image = global::Auto_parking.Properties.Resources.parkingcheckin;
            this.tsbtnCheckin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCheckin.Name = "tsbtnCheckin";
            this.tsbtnCheckin.Size = new System.Drawing.Size(111, 51);
            this.tsbtnCheckin.Text = "Check In";
            this.tsbtnCheckin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnCheckin.Click += new System.EventHandler(this.TsbtnCheckin_Click);
            // 
            // tsbtnAutoCheckin
            // 
            this.tsbtnAutoCheckin.Image = global::Auto_parking.Properties.Resources.parkingcheckin;
            this.tsbtnAutoCheckin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAutoCheckin.Name = "tsbtnAutoCheckin";
            this.tsbtnAutoCheckin.Size = new System.Drawing.Size(111, 51);
            this.tsbtnAutoCheckin.Text = "Auto Check In";
            this.tsbtnAutoCheckin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnAutoCheckin.Click += new System.EventHandler(this.TsbtnAutoCheckin_Click);
            // 
            // tsbtnCheckout
            // 
            this.tsbtnCheckout.Image = global::Auto_parking.Properties.Resources.parkingcheckout;
            this.tsbtnCheckout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCheckout.Name = "tsbtnCheckout";
            this.tsbtnCheckout.Size = new System.Drawing.Size(111, 51);
            this.tsbtnCheckout.Text = "Check Out";
            this.tsbtnCheckout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnCheckout.Click += new System.EventHandler(this.TsbtnCheckout_Click);
            // 
            // tsbtnParkingHistory
            // 
            this.tsbtnParkingHistory.Image = global::Auto_parking.Properties.Resources.parkinghistoryicon;
            this.tsbtnParkingHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnParkingHistory.Name = "tsbtnParkingHistory";
            this.tsbtnParkingHistory.Size = new System.Drawing.Size(111, 51);
            this.tsbtnParkingHistory.Text = "Parking History";
            this.tsbtnParkingHistory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnParkingHistory.Click += new System.EventHandler(this.TsbtnParkingHistory_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 10);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(101, 21);
            this.toolStripLabel3.Text = "SMS Setting";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::Auto_parking.Properties.Resources.parkinghistoryicon;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(111, 51);
            this.toolStripButton2.Text = "GSM Connection";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.ToolStripButton2_Click);
            // 
            // panelParent
            // 
            this.panelParent.BackgroundImage = global::Auto_parking.Properties.Resources.backgroundimageparking;
            this.panelParent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelParent.Controls.Add(this.btnLoginuser);
            this.panelParent.Controls.Add(this.btnLogin);
            this.panelParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelParent.Location = new System.Drawing.Point(114, 0);
            this.panelParent.Name = "panelParent";
            this.panelParent.Size = new System.Drawing.Size(930, 633);
            this.panelParent.TabIndex = 1;
            // 
            // btnLoginuser
            // 
            this.btnLoginuser.BackgroundImage = global::Auto_parking.Properties.Resources.login;
            this.btnLoginuser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLoginuser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginuser.Location = new System.Drawing.Point(3, 3);
            this.btnLoginuser.Name = "btnLoginuser";
            this.btnLoginuser.Size = new System.Drawing.Size(95, 99);
            this.btnLoginuser.TabIndex = 0;
            this.btnLoginuser.UseVisualStyleBackColor = true;
            this.btnLoginuser.Click += new System.EventHandler(this.BtnLoginuser_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackgroundImage = global::Auto_parking.Properties.Resources.logout;
            this.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Location = new System.Drawing.Point(0, 0);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(95, 99);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Visible = false;
            this.btnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 633);
            this.Controls.Add(this.panelParent);
            this.Controls.Add(this.hometoolsstrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Auto Car Parking System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HomeForm_Load);
            this.hometoolsstrip.ResumeLayout(false);
            this.hometoolsstrip.PerformLayout();
            this.panelParent.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripButton tsbtnCarTypes;
        private System.Windows.Forms.Panel panelParent;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbtnPersonRegistration;
        private System.Windows.Forms.ToolStripButton tsbtnAllVehicle;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton tsbtnCheckin;
        private System.Windows.Forms.ToolStripButton tsbtnCheckout;
        private System.Windows.Forms.ToolStripButton tsbtnParkingHistory;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton tsbtnAutoCheckin;
        public System.Windows.Forms.ToolStrip hometoolsstrip;
        public System.Windows.Forms.Button btnLogin;
        public System.Windows.Forms.Button btnLoginuser;
    }
}