﻿using Auto_parking.Forms.AutoTypesForm;
using Auto_parking.Forms.DriverTypesForm;
using Auto_parking.Forms.GSMForm;
using Auto_parking.Forms.ParkingForm;
using Auto_parking.Forms.RegistrationForm;
using Auto_parking.Forms.UserForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void ToolStripButton1_Click(object sender, EventArgs e)
        {
            frmDriverTypes DriverTypesForm = new frmDriverTypes();
            DriverTypesForm.ShowDialog();
        }

        private void TsbtnCarTypes_Click(object sender, EventArgs e)
        {
            frmAutoTypes AutoTypeForm = new frmAutoTypes();
            AutoTypeForm.ShowDialog();

        }

        private void TsbtnPersonRegistration_Click(object sender, EventArgs e)
        {
            PersonRegistration personRegistrationForm = new PersonRegistration();
            personRegistrationForm.ShowDialog();
        }

        private void TsbtnAllVehicle_Click(object sender, EventArgs e)
        {
            frmAllVichls AllVehicleForm = new frmAllVichls();
            AllVehicleForm.ShowDialog();
        }

        private void ToolStripButton2_Click(object sender, EventArgs e)
        {
            frmGSMConnection GSMConnection = new frmGSMConnection();
            GSMConnection.ShowDialog();
        }

        private void TsbtnCheckin_Click(object sender, EventArgs e)
        {
            frmCheckIn CheckInForm = new frmCheckIn();
            CheckInForm.ShowDialog();
        }

        private void TsbtnCheckout_Click(object sender, EventArgs e)
        {
            frmCheckOut CheckoutForm = new frmCheckOut();
            CheckoutForm.ShowDialog();
        }

        private void TsbtnParkingHistory_Click(object sender, EventArgs e)
        {
            frmParkingHistory parkingHistoryForm = new frmParkingHistory();
            parkingHistoryForm.ShowDialog();
        }

        private void TsbtnAutoCheckin_Click(object sender, EventArgs e)
        {
            MainForm form = new MainForm();
            form.ShowDialog();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
             
        private void HomeForm_Load(object sender, EventArgs e)
        {
            MainForm form = new MainForm();
            form.ShowDialog();
        }

        private void BtnLoginuser_Click(object sender, EventArgs e)
        {
            frmLogin LoginForm = new frmLogin(this);
            LoginForm.ShowDialog();
        }
    }
}
