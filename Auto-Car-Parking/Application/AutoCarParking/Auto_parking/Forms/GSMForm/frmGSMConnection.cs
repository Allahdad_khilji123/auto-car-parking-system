﻿using Auto_parking.SourceCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.GSMForm
{
    public partial class frmGSMConnection : Form
    {
        public frmGSMConnection()
        {
            InitializeComponent();
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            RetriveDevice();
            tBoxTimeOut.Text = "1500";
            tBoxBaudRate.Text = "19600";
        }

        private static List<USBDeviceInfo> GetUSBDevices()
        {

            List<USBDeviceInfo> devices = new List<USBDeviceInfo>();
            ManagementObjectCollection collection;
            ManagementObjectSearcher ManObjSearch;
            ManObjSearch = new ManagementObjectSearcher("Select * from Win32_SerialPort");
            collection = ManObjSearch.Get();
            foreach (var device in collection)
            {
                devices.Add(new USBDeviceInfo(
                (string)device.GetPropertyValue("DeviceID"),
                (string)device.GetPropertyValue("PNPDeviceID"),
                (string)device.GetPropertyValue("Name"),
                (string)device.GetPropertyValue("Caption"),
                (string)device.GetPropertyValue("Description"),
                (string)device.GetPropertyValue("ProviderType"),
                (string)device.GetPropertyValue("Status")
                ));
            }

            collection.Dispose();
            return devices;
        }

        private void RetriveDevice()
        {
            List<string> lsts = new List<string>();
            cmbPortNo.Items.Clear();
            if (cmbPortNo.Items.Count == 0)
            {

                var usbDevices = GetUSBDevices();
                string CmbItem = "";
                foreach (var usbDevice in usbDevices)
                {
                    CmbItem = "";
                    CmbItem = usbDevice.DeviceID + " (" + usbDevice.Name + ")";
                    cmbPortNo.Items.Add(CmbItem);
                }
                if (CmbItem.Trim().Length > 0)
                {
                    button3.Enabled = true;
                }
                else
                {
                    button3.Enabled = false;
                }

            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            epConfiguration.Clear();
            if (cmbPortNo.Text.Trim().Length == 0 || !cmbPortNo.Text.Trim().Contains("M"))
            {
                epConfiguration.SetError(cmbPortNo, "Please Connect Device or Enter our Device Port No.");
                cmbPortNo.Focus();
                cmbPortNo.SelectAll();
                return;
            }

            if (tBoxBaudRate.Text.Trim().Length == 0)
            {
                epConfiguration.SetError(tBoxBaudRate, "Please Enter BautRate or Press Reset label!");
                tBoxBaudRate.Focus();
                tBoxBaudRate.SelectAll();
                return;
            }
            if (tBoxTimeOut.Text.Trim().Length == 0)
            {
                epConfiguration.SetError(tBoxTimeOut, "Please Enter Timeout or Press Reset Label!");
                tBoxTimeOut.Focus();
                tBoxTimeOut.SelectAll();
                return;
            }
            int indx = cmbPortNo.Text.Trim().IndexOf("M");
            int COMNO = Convert.ToInt32(cmbPortNo.Text.Trim().Substring(indx + 1, indx));

            int bautRate = Convert.ToInt32(tBoxBaudRate.Text.Trim());
            int Timeout = Convert.ToInt32(tBoxTimeOut.Text.Trim());
            PortConfiguration.portNo = COMNO;
            PortConfiguration.bautRate = bautRate;
            PortConfiguration.Timeout = Timeout;
            MessageBox.Show("Connection is OK", "GSM System");
            this.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            RetriveDevice();
            tBoxTimeOut.Text = "1500";
            tBoxBaudRate.Text = "19600";
        }
    }
}
