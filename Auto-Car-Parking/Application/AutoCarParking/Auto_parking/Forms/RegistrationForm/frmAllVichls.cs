﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.RegistrationForm
{
    public partial class frmAllVichls : Form
    {
        public frmAllVichls()
        {
            InitializeComponent();
        }

        private void FillGrid(string searchvalue)
        {
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select PersonID [ID], FullName [Full Name], CNIC [CNIC], CellNo [Contact No], CarTypeID, CarType [Auto Type], DriverTypeID, DriverType [Driver Type], CarNo [Reg Flat No], RegDate [Reg Date], [Description] from v_PersonList ";
            }
            else
            {
                query = "select PersonID [ID], FullName [Full Name], CNIC [CNIC], CellNo [Contact No], CarTypeID, CarType [Auto Type], DriverTypeID, DriverType [Driver Type], CarNo [Reg Flat No], RegDate [Reg Date], [Description] from v_PersonList " +
                        "Where (FullName+' '+CNIC+' '+CellNo +' '+CarType +' '+DriverType+' '+ CarNo + ' ' +[Description]) like '%" + searchvalue.Trim() + "%'";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvPersonList.DataSource = dt;
                    dgvPersonList.Columns[0].Width = 100; // PersonID
                    dgvPersonList.Columns[1].Width = 200; // FullName
                    dgvPersonList.Columns[2].Width = 120; // CNIC
                    dgvPersonList.Columns[3].Width = 120; // CellNo
                    dgvPersonList.Columns[4].Visible = false; // CarTypeID
                    dgvPersonList.Columns[5].Width = 120; // CarType
                    dgvPersonList.Columns[6].Visible = false; // DriverTypeID
                    dgvPersonList.Columns[7].Width = 150; // DriverType
                    dgvPersonList.Columns[8].Width = 120; // CarNo
                    dgvPersonList.Columns[9].Width = 130; // RegDate
                    dgvPersonList.Columns[10].Width = 300; // Description
                }
            }
        }

        private void FrmAllVichls_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());    
        }

        private void dgvPersonList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
