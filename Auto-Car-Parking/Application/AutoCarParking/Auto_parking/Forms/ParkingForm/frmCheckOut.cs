﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.ParkingForm
{
    public partial class frmCheckOut : Form
    {
        public string Parkingid = string.Empty;
        public frmCheckOut()
        {
            InitializeComponent();
        }

        private void GetPersonInfo(string searchValue)
        {
            try
            {
                string query = "select ParkingID, PersonID, FullName,CarNo, CheckinDate, CheckinTime "
                        + ",CarType ,DriverType ,SRTCode  from v_ParkingList where CheckoutDate IS NULL AND CheckoutTime IS NULL AND SRTCode = '" + searchValue + "'";
                DataTable dt = DatabaseAccess.Select(query);
                if (dt != null)
                {
                    if (dt.Rows.Count == 1)
                    {
                        Parkingid = dt.Rows[0]["ParkingID"].ToString(); 
                        lblPersonName.Text = dt.Rows[0]["FullName"].ToString();
                        lblDriverType.Text = dt.Rows[0]["DriverType"].ToString();
                        lblAutoType.Text = dt.Rows[0]["CarType"].ToString();
                        lblNoFlat.Text = dt.Rows[0]["CarNo"].ToString();
                        lblCheckInDate.Text = dt.Rows[0]["CheckinDate"].ToString();
                        lblCheckInTime.Text = dt.Rows[0]["CheckinTime"].ToString();
                        return;
                    }
                    else
                    {
                        Parkingid = string.Empty;
                        lblPersonName.Text ="";
                        lblDriverType.Text = "";
                        lblAutoType.Text = "";
                        lblNoFlat.Text = "";
                        lblCheckInDate.Text = "";
                        lblCheckInTime.Text = "";
                        MessageBox.Show("Not Found!");
                    }
                }
                else
                {
                    Parkingid = string.Empty;
                    lblPersonName.Text = "";
                    lblDriverType.Text = "";
                    lblAutoType.Text = "";
                    lblNoFlat.Text = "";
                    lblCheckInDate.Text = "";
                    lblCheckInTime.Text = "";
                    MessageBox.Show("Not Found!");
                }
            }
            catch
            {

                Parkingid = string.Empty;
                lblPersonName.Text = "";
                lblDriverType.Text = "";
                lblAutoType.Text = "";
                lblNoFlat.Text = "";
                lblCheckInDate.Text = "";
                lblCheckInTime.Text = "";
                MessageBox.Show("Not Found!");
            }
        }

        private void FrmCheckOut_Load(object sender, EventArgs e)
        {

        }

        private void TxtCheckOutCode_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnCheckout_Click(object sender, EventArgs e)
        {  
            if (string.IsNullOrEmpty(Parkingid))
            {
                MessageBox.Show("Please Enter Correct Check Out Code!");
                Parkingid = string.Empty;
                lblPersonName.Text = "";
                lblDriverType.Text = "";
                lblAutoType.Text = "";
                lblNoFlat.Text = "";
                lblCheckInDate.Text = "";
                lblCheckInTime.Text = "";
                return;
            }

            DataTable dt = DatabaseAccess.Select("select * from ParkingTable where ParkingID = '" + Parkingid + "' AND CheckoutDate IS NOT NULL AND CheckoutTime IS NOT NULL");
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("It's Already Check Out!");
                    return;
                }
            }

            string updatequery = string.Format("update ParkingTable set CheckoutDate = '{0}', CheckoutTime = '{1}' Where ParkingID = '{2}'",
                 DateTime.Now.ToString("yyyy/MM/dd"), DateTime.Now.ToString("HH:mm"), Parkingid);
            bool result = DatabaseAccess.InsertUpdateDelete(updatequery);
            if (result == true)
            {
                MessageBox.Show("Check Out Successfully!");
            }
            else
            {
                MessageBox.Show("Please try Again!");
            }
        }

        private void TxtCheckOutCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GetPersonInfo(txtCheckOutCode.Text.Trim());
            }
        }
    }
}
