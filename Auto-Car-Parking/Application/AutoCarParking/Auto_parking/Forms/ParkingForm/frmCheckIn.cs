﻿using Auto_parking.SourceCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.ParkingForm
{
    public partial class frmCheckIn : Form
    {
        public string PersonID = string.Empty;
        public frmCheckIn()
        {
            InitializeComponent();
        }


        private void FillGrid(string searchvalue)
        {
            dgvCheckIn.DataSource = null;
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select PersonID [ID], FullName [Full Name],CarNo [No Flat],CheckinDate [Check In Date],CheckinTime [ Check In Time] "
                        + ",CarType [Auto Type],DriverType [Driver Type],SRTCode [Check Out Code] from v_ParkingList where CheckoutDate IS NULL AND CheckoutTime IS NULL"; ;
            }
            else
            {
                query = "select PersonID [ID], FullName [Full Name],CarNo [No Flat],CheckinDate [Check In Date],CheckinTime [ Check In Time] "
                        + ",CarType [Auto Type],DriverType [Driver Type],SRTCode [Check Out Code] from v_ParkingList where CheckoutDate IS NULL AND CheckoutTime IS NULL AND CarNo = '" + searchvalue + "'";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvCheckIn.DataSource = dt;
                    dgvCheckIn.Columns[0].Width = 100; // PersonID
                    dgvCheckIn.Columns[1].Width = 120;  // FullName
                    dgvCheckIn.Columns[2].Width = 120; // CarNo
                    dgvCheckIn.Columns[3].Width = 120; // CheckinDate
                    dgvCheckIn.Columns[4].Width = 100; // CheckinTime
                    dgvCheckIn.Columns[5].Width = 100; // CarType
                    dgvCheckIn.Columns[6].Width = 100; // DriverType
                    dgvCheckIn.Columns[7].Width = 120; // SRTCode
              
                }
            }


        }

        private void GetPersonInfo( string searchValue)
        {
            try
            {
                string query = string.Format("select PersonID,FullName,CellNo from PersonTable where CarNo = '" + searchValue + "'");
                DataTable dt = DatabaseAccess.Select(query);
                if (dt != null)
                {
                    if (dt.Rows.Count == 1)
                    {
                        PersonID = dt.Rows[0][0].ToString();
                        lblPersonName.Text = dt.Rows[0][1].ToString();
                        lblContactNo.Text = dt.Rows[0][2].ToString();
                        return;
                    }
                    else
                    {
                        PersonID = string.Empty;
                        lblPersonName.Text = "";
                        lblContactNo.Text = "";
                    }
                }
                else
                {
                    PersonID = string.Empty;
                    lblPersonName.Text = "";
                    lblContactNo.Text = "";
                }
            }
            catch  
            {

                PersonID = string.Empty;
                lblPersonName.Text = "";
                lblContactNo.Text = "";
            }
        }

        private void TxtNoFlat_TextChanged(object sender, EventArgs e)
        {
            GetPersonInfo(txtNoFlat.Text.Trim());
        }

        private void BtnCheckIn_Click(object sender, EventArgs e)
        {
            try
            {  
                if (string.IsNullOrEmpty(PersonID))
                {
                    MessageBox.Show("Please Enter Correct No-Flat!");
                    return;
                }

                string SRTCode = GetCodeForParking();
                DataTable dt = DatabaseAccess.Select("select * from ParkingTable where PersonID = '" + PersonID + "' AND CheckoutDate IS NULL AND CheckoutTime IS NULL");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show("It's Already in Parking!");
                        return;
                    }
                }

                string insertquery = string.Format("insert into ParkingTable(PersonID, CheckinDate, CheckinTime, SRTCode) values('{0}', '{1}', '{2}', '{3}')",
                    PersonID, DateTime.Now.ToString("yyyy/MM/dd"), DateTime.Now.ToString("HH:mm"), SRTCode);
                bool result = DatabaseAccess.InsertUpdateDelete(insertquery);
                if (result == true)
                {

                    MessageBox.Show("Check In Successfully!");
                    FillGrid(string.Empty);
                    
                    string ContactNo = lblContactNo.Text.Trim().Replace("+92", "0");
                    ContactNo = ContactNo.Replace(" ", "");
                    ContactNo = ContactNo.Replace("-", "");
                   string SMStext = "No Flat : " + txtNoFlat.Text.Trim() + " Check out code : " + SRTCode;
                    if (ContactNo.Trim().Length > 1)
                    {
                        PortConfiguration CheckPort = new PortConfiguration();
                        if (Convert.ToString(PortConfiguration.portNo).Length > 0 && Convert.ToString(PortConfiguration.bautRate).Length > 0 && Convert.ToString(PortConfiguration.Timeout).Length > 0)
                        {
                            string SMSStatus = GSMConfiguration.SendSMS5MinTest("(" + SMStext + ") \n Check In Date :" + DateTime.Now.ToString("dd MMMM yyyy") + " \n \n ", ContactNo.Trim(), PortConfiguration.portNo, PortConfiguration.bautRate, PortConfiguration.Timeout);
                            if (SMSStatus == "sucess")
                            {
                                MessageBox.Show("Sending Complete", "GSM System");
                                return;
                            }
                            if (SMSStatus == "sending Failed")
                            {
                                MessageBox.Show("Check the Connection and Try Again.", "GSM System");
                                return;
                            }

                            if (SMSStatus != "sending Failed" || SMSStatus != "sucess")
                            {
                                MessageBox.Show("Please First Establish GSM Connection.", "GSM System");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Check Connection or Contact No!", "Active Chat System.");
                    }
                    txtNoFlat.Clear();
                }
                else
                {
                    txtNoFlat.Clear();
                    MessageBox.Show("Please try Again!");
                }
            }
            catch  
            {
                MessageBox.Show("Some un-expected issue is occure, please contact to IT Team!");
            }

        }

        // define characters allowed in passcode.  set length so divisible into 256
        static char[] ValidChars = {'2','3','4','5','6','7','8','9',
                   'A','B','C','D','E','F','G','H',
                   'J','K','L','M','N','P','Q',
                   'R','S','T','U','V','W','X','Y','Z'}; // len=32

        const string hashkey = "password"; //key for HMAC function -- change!
        const int codelength = 4; // lenth of passcode

        string GetCodeForParking()
        {
            byte[] hash;
            using (HMACSHA1 sha1 = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(hashkey)))
                hash = sha1.ComputeHash(UTF8Encoding.UTF8.GetBytes(DateTime.Now.ToString()));
            int startpos = hash[hash.Length - 1] % (hash.Length - codelength);
            StringBuilder passbuilder = new StringBuilder();
            for (int i = startpos; i < startpos + codelength; i++)
                passbuilder.Append(ValidChars[hash[i] % ValidChars.Length]);
            return passbuilder.ToString();
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
        }

        private void FrmCheckIn_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);
        }

        private void SendParkingCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvCheckIn != null)
            {
                if (dgvCheckIn.Rows.Count > 0)
                {
                    if (dgvCheckIn.SelectedRows.Count == 1)
                    {
                        DataTable dt = DatabaseAccess.Select("select top 1 * from PersonTable where PersonID = '" + Convert.ToString(dgvCheckIn.CurrentRow.Cells[0].Value).Trim() + "'");
                        if (dt != null)
                        {
                            string ContactNo = Convert.ToString(dt.Rows[0][5].ToString().Trim());
                            ContactNo.Trim().Replace("+92", "0");
                            ContactNo = ContactNo.Replace(" ", "");
                            ContactNo = ContactNo.Replace("-", "");
                            string SMStext = "No Flat : " + Convert.ToString(dgvCheckIn.CurrentRow.Cells[2].Value) + " Check out code : " + Convert.ToString(dgvCheckIn.CurrentRow.Cells[7].Value);
                            if (ContactNo.Trim().Length > 1)
                            {
                                PortConfiguration CheckPort = new PortConfiguration();
                                if (Convert.ToString(PortConfiguration.portNo).Length > 0 && Convert.ToString(PortConfiguration.bautRate).Length > 0 && Convert.ToString(PortConfiguration.Timeout).Length > 0)
                                {
                                    string SMSStatus = GSMConfiguration.SendSMS5MinTest("(" + SMStext + ") \n Check In Date :" + DateTime.Now.ToString("dd MMMM yyyy") + " \n \n ", ContactNo.Trim(), PortConfiguration.portNo, PortConfiguration.bautRate, PortConfiguration.Timeout);
                                    if (SMSStatus == "sucess")
                                    {
                                        MessageBox.Show("Sending Complete", "GSM System");
                                        return;
                                    }
                                    if (SMSStatus == "sending Failed")
                                    {
                                        MessageBox.Show("Check the Connection and Try Again.", "GSM System");
                                        return;
                                    }

                                    if (SMSStatus != "sending Failed" || SMSStatus != "sucess")
                                    {
                                        MessageBox.Show("Please First Establish GSM Connection.", "GSM System");
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please Check Connection or Contact No!", "Active Chat System.");
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("?", "Please Select One Record!");
                    }
                }
                else
                {
                    MessageBox.Show("?","No Record Found!");
                }
            }
        }
    }
}
