﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.DriverTypesForm
{
    public partial class frmDriverTypes : Form
    {
        public frmDriverTypes()
        {
            InitializeComponent();
        }

        private void FillGrid(string searchvalue)
        {
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select DriverTypeID [ID] , DriverType [Driver Type], [Description] from DriverTypeTable";
            }
            else
            {
                query = "select DriverTypeID[ID] , DriverType[Driver Type], [Description] from DriverTypeTable " +
                         " Where(DriverType + ' ' + Description) Like '%"+searchvalue.Trim()+"%';";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvDriverTypes.DataSource = dt;
                    dgvDriverTypes.Columns[0].Width = 100;
                    dgvDriverTypes.Columns[1].Width = 200;
                    dgvDriverTypes.Columns[2].Width = 300;
                }
            }


        }

        private void ClearForm()
        {
            txtDescription.Clear();
            txtDriverType.Clear();
            txtSearch.Clear();
        }

        private void EnableControls()
        {
            btnSave.Enabled = false;
            txtSearch.Enabled = false;
            dgvDriverTypes.Enabled = false;
            btnCancel.Enabled = true;
            btnUpdate.Enabled = true;
        }

        private void DisableControls()
        {
            btnSave.Enabled = true;
            txtSearch.Enabled = true;
            dgvDriverTypes.Enabled = true;
            btnCancel.Enabled = false;
            btnUpdate.Enabled = false;
            ClearForm();
            FillGrid(string.Empty);
        }

        private void FrmDriverTypes_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DisableControls();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {    
            try
            {
                ep.Clear();
                if (txtDriverType.Text.Trim().Length == 0)
                {
                    ep.SetError(txtDriverType, "Required Field!");
                    txtDriverType.Focus();
                    return;
                }

                DataTable dt = DatabaseAccess.Select("select * from DriverTypeTable where DriverType = '" + txtDriverType.Text.Trim() + "'");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ep.SetError(txtDriverType, "Already Exist!");
                        txtDriverType.SelectAll();
                        txtDriverType.Focus();
                        return;
                    }
                }

                string insertquery = string.Format("insert into DriverTypeTable(DriverType,Description) values('{0}','{1}')", txtDriverType.Text.Trim(), txtDescription.Text.Trim());
                bool result = DatabaseAccess.InsertUpdateDelete(insertquery);
                if (result == true)
                {
                    ClearForm();
                    FillGrid(string.Empty);
                    MessageBox.Show("Save Successfully");
                }
                else
                {
                    MessageBox.Show("Please Try Again!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvDriverTypes != null)
            {
                if (dgvDriverTypes.Rows.Count > 0)
                {
                    if (dgvDriverTypes.SelectedRows.Count == 1)
                    {
                        txtDriverType.Text = Convert.ToString(dgvDriverTypes.CurrentRow.Cells[1].Value);
                        txtDescription.Text = Convert.ToString(dgvDriverTypes.CurrentRow.Cells[2].Value);
                        EnableControls();
                    }
                    else {
                        MessageBox.Show("Please Select One Record!");
                    }
                }
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ep.Clear();
                if (txtDriverType.Text.Trim().Length == 0)
                {
                    ep.SetError(txtDriverType, "Required Field!");
                    txtDriverType.Focus();
                    return;
                }

                DataTable dt = DatabaseAccess.Select("select * from DriverTypeTable where DriverType = '" + txtDriverType.Text.Trim() + "' and DriverTypeID != "+Convert.ToString(dgvDriverTypes.CurrentRow.Cells[0].Value)+"");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ep.SetError(txtDriverType, "Already Exist!");
                        txtDriverType.SelectAll();
                        txtDriverType.Focus();
                        return;
                    }
                }

                string updatequery = string.Format("update DriverTypeTable set DriverType = '{0}',Description = '{1}' where DriverTypeID = '{2}'", txtDriverType.Text.Trim(), txtDescription.Text.Trim(), Convert.ToString(dgvDriverTypes.CurrentRow.Cells[0].Value));
                bool result = DatabaseAccess.InsertUpdateDelete(updatequery);
                if (result == true)
                {
                    ClearForm();
                    FillGrid(string.Empty);
                    DisableControls();
                    MessageBox.Show("Updated Successfully");
                }
                else
                {
                    MessageBox.Show("Please Try Again!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
