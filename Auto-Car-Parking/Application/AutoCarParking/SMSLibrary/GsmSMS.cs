﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSI;
using MSI.PduConverter;
using MSI.PduConverter.SmartMessaging;
using MSI.GsmCommunication;
using MSI.Interfaces;
using System.Data;
using System.Collections.Specialized;
 


namespace SMSLibrary
{
    public class GsmSMS
    {
        
        public int port { get; set; }
        public int baudrate { get; set; }
        public int timeout { get; set; }


        public GsmSMS()
        {

            this.baudrate = 19600;
            this.timeout = 1500;
            this.port = 7;
        }
        public GsmSMS(int port, int baudrate, int timeout)
        {

            this.timeout = timeout;
            this.port = port;
            this.baudrate = baudrate;
        }


    }
    public class ManageSmsGsm
    {
        public static ushort refNumber = 1;
        GsmCommMain gCom;


        public GsmCommMain SetGsmconnection(GsmSMS gcon = null)
        {
            //bool b = false;
            //string result = "";
            //try
            //{
            //    if (gcon == null)
            //    {
            //        gcon = new GsmSMS();
            gCom = new GsmCommMain(gcon.port, gcon.baudrate, gcon.timeout);

            //        result = OpenGsmCom(gCom);
            //        if (result=="1")
            //        {
            //            if (isConnectGSM(gCom))
            return gCom;
            //            else
            //                throw new Exception();
            //        }
            //        else
            //            throw new Exception();

            //    }

            //    else
            //    {
            //        // return gcon;
            //        gCom = new GsmCommMain(gcon.port, gcon.baudrate, gcon.timeout);
            //        result = OpenGsmCom(gCom);
            //        if (result == "1")
            //        {
            //            if (isConnectGSM(gCom))
            //                return gCom;
            //            else
            //                throw new Exception();
            //        }
            //        else
            //            throw new Exception();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(result);
            //}
            //    /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //finally
            //{
            //    gCom.Close();

            //}
        }

        /*public GsmCommMain SetGsmconnection(bool db)
        {

            try
            {

                if (db == true)
                {
                    DataSet ds = GsmComSetting.SelectSetting();
                    if (ds.Tables[0].Rows.Count >= 1)
                    {
                        //if (gCom != null)
                        //{

                        //    gCom.Close();
                        //}
                        gCom = new GsmCommMain(Convert.ToInt32(ds.Tables[0].Rows[0]["com"]), Convert.ToInt32(ds.Tables[0].Rows[0]["baudrate"]), Convert.ToInt32(ds.Tables[0].Rows[0]["timeout"]));
                        return gCom;

                    }
                    else
                    {

                        throw new Exception();
                    }

                }


                else
                {

                    throw new Exception();

                }
            }
            catch (Exception ex)
            {
                // throw new Exception();
                return null;
            }
        }*/
        public bool OpenGsmCom(GsmCommMain gcom)
        {
            try
            {


                if (!gcom.IsOpen())
                {
                    // gcom.Close();
                    gcom.Open();
                    return true;
                }
                else
                {

                    return false;
                }

            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool isConnectGSM(GsmCommMain gcom)
        {

            try
            {
                if (gcom.IsConnected())
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool SendSMS(GsmCommMain gcom, SMSContents smscont)
        {
            try
            {
                if (gcom != null)
                {
                    SmsSubmitPdu smspduone = new SmsSubmitPdu(smscont.smsText.ToString(), smscont.reciverNumber.ToString());
                    smspduone.RequestStatusReport = true;
                    gcom.SendMessage(smspduone);

                    return true;

                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                return false;
            }
            finally
            {
                
                gCom.Close();
            }
        }

        public bool SendSMSQueu(GsmCommMain gcom, List<SMSContents> smsconts)
        {

            try
            {
                if (gcom != null)
                {
                    if (!gcom.IsOpen())
                    {
                        gcom.Open();
                        if (gcom.IsConnected())
                        {
                            foreach (SMSContents smscont in smsconts)
                            {
                                OutgoingSmsPdu[] pds = SmartMessageFactory.CreateConcatTextMessage(smscont.smsText.Trim(), false, smscont.reciverNumber);
                                foreach (OutgoingSmsPdu pdu in pds)
                                {
                                    gcom.SendMessage(pdu);
                                }


                            }

                            // data here in db

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (gcom.IsConnected())
                        {
                            foreach (SMSContents smscont in smsconts)
                            {
                                OutgoingSmsPdu[] pds = SmartMessageFactory.CreateConcatTextMessage(smscont.smsText.Trim(), false, smscont.reciverNumber);
                                foreach (OutgoingSmsPdu pdu in pds)
                                {
                                    gcom.SendMessage(pdu);
                                }
                            }


                            return true;
                        }
                        else
                        {

                            return false;
                        }

                    }

                }
                else
                {


                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
            finally
            {
                gcom.Close();
                gcom = null;
            }
        }
        // same msg to whole class
        public bool SendSMSToMultipul(GsmCommMain gcom, string msgtext, string[] recivernumbers)
        {

            try
            {    
                       
               foreach (string num in recivernumbers)
               {
                   OutgoingSmsPdu[] pdus = MSI.PduConverter.SmartMessaging.SmartMessageFactory.CreateConcatTextMessage(msgtext, false, num);
                   foreach (OutgoingSmsPdu pdu in pdus)
                   {
                       gcom.SendMessage(pdu);
                   }
               }
           
               return true;
               
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        /////// Send cancatenated SMS's
       
 public static SmsSubmitPdu[] CreateConcatTextMessage(string userDataText, bool unicode, string destinationAddress)
{
	int num = unicode ? 70 : 160;
	string text;
	if (!unicode)
	{
		text = TextDataConverter.StringTo7Bit(userDataText);
	}
	else
	{
		text = userDataText;
	}
	if (text.Length <= num)
	{
		SmsSubmitPdu smsSubmitPdu;
		if (!unicode)
		{
			smsSubmitPdu = new SmsSubmitPdu(userDataText, destinationAddress);
		}
		else
		{
			smsSubmitPdu = new SmsSubmitPdu(userDataText, destinationAddress, 8);
		}
		return new SmsSubmitPdu[]
		{
			smsSubmitPdu
		};
	}
	ConcatMessageElement16 element = new ConcatMessageElement16(0, 0, 0);
	byte b = (byte)SmartMessageFactory.CreateUserDataHeader(element).Length;
	byte b2 = (byte)((double)b / 7.0 * 8.0);
	byte b3 = unicode ? b : b2;
	StringCollection stringCollection = new StringCollection();
	int num2;
	for (int i = 0; i < text.Length; i += num2)
	{
		string value = string.Empty;
		if (unicode)
		{
			if (text.Length - i >= (num * 2 - (int)b3) / 2)
			{
				num2 = (num * 2 - (int)b3) / 2;
			}
			else
			{
				num2 = text.Length - i;
			}
		}
		else
		{
			if (text.Length - i >= num - (int)b3)
			{
				num2 = num - (int)b3;
			}
			else
			{
				num2 = text.Length - i;
			}
		}
		value = text.Substring(i, num2);
		stringCollection.Add(value);
	}
	if (stringCollection.Count > 255)
	{
		throw new ArgumentException("A concatenated message must not have more than 255 parts.", "userDataText");
	}
	SmsSubmitPdu[] array = new SmsSubmitPdu[stringCollection.Count];
	ushort referenceNumber = CalcNextRefNumber();
	byte b4 = 0;
	for (int j = 0; j < stringCollection.Count; j++)
	{
		b4 += 1;
		ConcatMessageElement16 element2 = new ConcatMessageElement16(referenceNumber, (byte)stringCollection.Count, b4);
		byte[] header = SmartMessageFactory.CreateUserDataHeader(element2);
		byte[] array2;
		int num3;
		if (!unicode)
		{
			array2 = TextDataConverter.SeptetsToOctetsInt(stringCollection[j]);
			num3 = stringCollection[j].Length;
		}
		else
		{
			Encoding bigEndianUnicode = Encoding.BigEndianUnicode;
			array2 = bigEndianUnicode.GetBytes(stringCollection[j]);
			num3 = array2.Length;
		}
		SmsSubmitPdu smsSubmitPdu2 = new SmsSubmitPdu();
		smsSubmitPdu2.DestinationAddress = destinationAddress;
		if (unicode)
		{
			smsSubmitPdu2.DataCodingScheme = 8;
		}
		smsSubmitPdu2.SetUserData(array2, (byte)num3);
		smsSubmitPdu2.AddUserDataHeader(header);
		array[j] = smsSubmitPdu2;
	}
	return array;
}

 protected static ushort CalcNextRefNumber()
{
	ushort result;
	lock (typeof(SmartMessageFactory))
	{
		result = refNumber;
		if (refNumber == 65535)
		{
			refNumber = 1;
		}
		else
		{
			refNumber += 1;
		}
	}
	return result;
}


     
        public bool SendLargeSMS(string largesms, GsmCommMain gcom, string number)
        {
             OutgoingSmsPdu[] pdus = MSI.PduConverter.SmartMessaging.SmartMessageFactory.CreateConcatTextMessage(largesms, false, number);
            int num = pdus.Length;
            if (num >= 1)
            {
                try
                {


                    foreach (OutgoingSmsPdu pdu in pdus)
                    {
                        gcom.SendMessage(pdu);
                    }
                   
                   
                    return true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //return false;
                }

            }
            else
            {

                return false;
            }


        }
 
         
    }
}
