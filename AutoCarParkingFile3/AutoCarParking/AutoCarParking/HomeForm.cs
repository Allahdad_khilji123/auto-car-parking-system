﻿using Auto_parking.Forms.AutoTypesForm;
using Auto_parking.Forms.DriverTypesForm;
using Auto_parking.Forms.UserForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCarParking
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void tsbtnCarTypes_Click(object sender, EventArgs e)
        {
            frmAutoTypes AutoTypeForm = new frmAutoTypes();
            AutoTypeForm.ShowDialog();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmDriverTypes DriverTypesForm = new frmDriverTypes();
            DriverTypesForm.ShowDialog();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            frmLogin LoginForm = new frmLogin(this);
            LoginForm.ShowDialog();
        }
    }
}
