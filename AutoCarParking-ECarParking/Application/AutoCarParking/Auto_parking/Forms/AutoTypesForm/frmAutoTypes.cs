﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.AutoTypesForm
{
    public partial class frmAutoTypes : Form
    {
        public frmAutoTypes()
        {
            InitializeComponent();
        }

        private void FillGrid(string searchvalue)
        {
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select CarTypeID [ID] , CarType [Auto Type], [Description] from CarTypeTable";
            }
            else
            {
                query = "select CarTypeID [ID] , CarType [Auto Type], [Description] from CarTypeTable " +
                         " Where(CarType + ' ' + Description) Like '%" + searchvalue.Trim() + "%';";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvAutoTypes.DataSource = dt;
                    dgvAutoTypes.Columns[0].Width = 100;
                    dgvAutoTypes.Columns[1].Width = 200;
                    dgvAutoTypes.Columns[2].Width = 300;
                }
            }


        }

        private void ClearForm()
        {
            txtDescription.Clear();
            txtAutoType.Clear();
            txtSearch.Clear();
        }

        private void EnableControls()
        {
            btnSave.Enabled = false;
            txtSearch.Enabled = false;
            dgvAutoTypes.Enabled = false;
            btnCancel.Enabled = true;
            btnUpdate.Enabled = true;
        }

        private void DisableControls()
        {
            btnSave.Enabled = true;
            txtSearch.Enabled = true;
            dgvAutoTypes.Enabled = true;
            btnCancel.Enabled = false;
            btnUpdate.Enabled = false;
            ClearForm();
            FillGrid(string.Empty);
        }


        private void FrmAutoTypes_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DisableControls();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
          
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ep.Clear();
                if (txtAutoType.Text.Trim().Length == 0)
                {
                    ep.SetError(txtAutoType, "Required Field!");
                    txtAutoType.Focus();
                    return;
                }

                DataTable dt = DatabaseAccess.Select("select * from CarTypeTable where CarType = '" + txtAutoType.Text.Trim() + "'");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ep.SetError(txtAutoType, "Already Exist!");
                        txtAutoType.SelectAll();
                        txtAutoType.Focus();
                        return;
                    }
                }

                string insertquery = string.Format("insert into CarTypeTable(CarType,Description) values('{0}','{1}')", txtAutoType.Text.Trim(), txtDescription.Text.Trim());
                bool result = DatabaseAccess.InsertUpdateDelete(insertquery);
                if (result == true)
                {
                    ClearForm();
                    FillGrid(string.Empty);
                    MessageBox.Show("Save Successfully");
                }
                else
                {
                    MessageBox.Show("Please Try Again!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvAutoTypes != null)
            {
                if (dgvAutoTypes.Rows.Count > 0)
                {
                    if (dgvAutoTypes.SelectedRows.Count == 1)
                    {
                        txtAutoType.Text = Convert.ToString(dgvAutoTypes.CurrentRow.Cells[1].Value);
                        txtDescription.Text = Convert.ToString(dgvAutoTypes.CurrentRow.Cells[2].Value);
                        EnableControls();
                    }
                    else
                    {
                        MessageBox.Show("Please Select One Record!");
                    }
                }
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ep.Clear();
                if (txtAutoType.Text.Trim().Length == 0)
                {
                    ep.SetError(txtAutoType, "Required Field!");
                    txtAutoType.Focus();
                    return;
                }

                DataTable dt = DatabaseAccess.Select("select * from CarTypeTable where CarType = '" + txtAutoType.Text.Trim() + "' and CarTypeID != " + Convert.ToString(dgvAutoTypes.CurrentRow.Cells[0].Value) + "");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ep.SetError(txtAutoType, "Already Exist!");
                        txtAutoType.SelectAll();
                        txtAutoType.Focus();
                        return;
                    }
                }

                string updatequery = string.Format("update CarTypeTable set CarType = '{0}',Description = '{1}' where CarTypeID = '{2}'", txtAutoType.Text.Trim(), txtDescription.Text.Trim(), Convert.ToString(dgvAutoTypes.CurrentRow.Cells[0].Value));
                bool result = DatabaseAccess.InsertUpdateDelete(updatequery);
                if (result == true)
                {
                    ClearForm();
                    FillGrid(string.Empty);
                    DisableControls();
                    MessageBox.Show("Updated Successfully");
                }
                else
                {
                    MessageBox.Show("Please Try Again!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
