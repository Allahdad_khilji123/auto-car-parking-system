﻿namespace Auto_parking.Forms.ParkingForm
{
    partial class frmCheckOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheckout = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCheckOutCode = new System.Windows.Forms.TextBox();
            this.lbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCheckInTime = new System.Windows.Forms.Label();
            this.lblCheckInDate = new System.Windows.Forms.Label();
            this.lblNoFlat = new System.Windows.Forms.Label();
            this.lblAutoType = new System.Windows.Forms.Label();
            this.lblDriverType = new System.Windows.Forms.Label();
            this.lblPersonName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCheckout
            // 
            this.btnCheckout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckout.Location = new System.Drawing.Point(223, 318);
            this.btnCheckout.Name = "btnCheckout";
            this.btnCheckout.Size = new System.Drawing.Size(137, 46);
            this.btnCheckout.TabIndex = 4;
            this.btnCheckout.Text = "Check Out";
            this.btnCheckout.UseVisualStyleBackColor = true;
            this.btnCheckout.Click += new System.EventHandler(this.BtnCheckout_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCheckout);
            this.groupBox1.Controls.Add(this.txtCheckOutCode);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblCheckInTime);
            this.groupBox1.Controls.Add(this.lblCheckInDate);
            this.groupBox1.Controls.Add(this.lblNoFlat);
            this.groupBox1.Controls.Add(this.lblAutoType);
            this.groupBox1.Controls.Add(this.lblDriverType);
            this.groupBox1.Controls.Add(this.lblPersonName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 378);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter Check In Details";
            // 
            // txtCheckOutCode
            // 
            this.txtCheckOutCode.Location = new System.Drawing.Point(153, 32);
            this.txtCheckOutCode.Name = "txtCheckOutCode";
            this.txtCheckOutCode.Size = new System.Drawing.Size(208, 20);
            this.txtCheckOutCode.TabIndex = 1;
            this.txtCheckOutCode.TextChanged += new System.EventHandler(this.TxtCheckOutCode_TextChanged);
            this.txtCheckOutCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtCheckOutCode_KeyDown);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(17, 281);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(82, 13);
            this.lbl.TabIndex = 0;
            this.lbl.Text = "Check In Time :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 240);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Check In Date :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "No Flat :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Auto Type :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Driver Type :";
            // 
            // lblCheckInTime
            // 
            this.lblCheckInTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCheckInTime.Location = new System.Drawing.Point(104, 275);
            this.lblCheckInTime.Name = "lblCheckInTime";
            this.lblCheckInTime.Size = new System.Drawing.Size(257, 27);
            this.lblCheckInTime.TabIndex = 0;
            // 
            // lblCheckInDate
            // 
            this.lblCheckInDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCheckInDate.Location = new System.Drawing.Point(104, 235);
            this.lblCheckInDate.Name = "lblCheckInDate";
            this.lblCheckInDate.Size = new System.Drawing.Size(257, 27);
            this.lblCheckInDate.TabIndex = 0;
            // 
            // lblNoFlat
            // 
            this.lblNoFlat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNoFlat.Location = new System.Drawing.Point(103, 189);
            this.lblNoFlat.Name = "lblNoFlat";
            this.lblNoFlat.Size = new System.Drawing.Size(257, 27);
            this.lblNoFlat.TabIndex = 0;
            // 
            // lblAutoType
            // 
            this.lblAutoType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAutoType.Location = new System.Drawing.Point(104, 148);
            this.lblAutoType.Name = "lblAutoType";
            this.lblAutoType.Size = new System.Drawing.Size(257, 27);
            this.lblAutoType.TabIndex = 0;
            // 
            // lblDriverType
            // 
            this.lblDriverType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDriverType.Location = new System.Drawing.Point(103, 109);
            this.lblDriverType.Name = "lblDriverType";
            this.lblDriverType.Size = new System.Drawing.Size(257, 27);
            this.lblDriverType.TabIndex = 0;
            // 
            // lblPersonName
            // 
            this.lblPersonName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPersonName.Location = new System.Drawing.Point(104, 66);
            this.lblPersonName.Name = "lblPersonName";
            this.lblPersonName.Size = new System.Drawing.Size(257, 27);
            this.lblPersonName.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Person Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Enter Check OUT Code :";
            // 
            // frmCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 403);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCheckOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Check Out";
            this.Load += new System.EventHandler(this.FrmCheckOut_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCheckout;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCheckOutCode;
        private System.Windows.Forms.Label lblPersonName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNoFlat;
        private System.Windows.Forms.Label lblAutoType;
        private System.Windows.Forms.Label lblDriverType;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCheckInTime;
        private System.Windows.Forms.Label lblCheckInDate;
    }
}