﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.ParkingForm
{
    public partial class frmParkingHistory : Form
    {
        public frmParkingHistory()
        {
            InitializeComponent();
        }

        private void FillGrid(string searchvalue)
        {
            dgvCheckIn.DataSource = null;
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select PersonID [ID], FullName [Full Name],CarNo [No Flat],CheckinDate [Check In Date],CheckinTime [ Check In Time], CheckoutDate [Check Out Date], CheckoutTime [Check out Time] "
                        + ",CarType [Auto Type],DriverType [Driver Type],SRTCode [Check Out Code] from v_ParkingList";
            }
            else
            {
                query = "select PersonID [ID], FullName [Full Name],CarNo [No Flat],CheckinDate [Check In Date],CheckinTime [ Check In Time] , CheckoutDate [Check Out Date], CheckoutTime [Check out Time] "
                        + ",CarType [Auto Type],DriverType [Driver Type] from v_ParkingList where CarNo = '" + searchvalue + "'";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvCheckIn.DataSource = dt;
                    dgvCheckIn.Columns[0].Visible = false; // PersonID
                    dgvCheckIn.Columns[1].Width = 120;  // FullName
                    dgvCheckIn.Columns[2].Width = 120; // CarNo
                    dgvCheckIn.Columns[3].Width = 120; // CheckinDate
                    dgvCheckIn.Columns[4].Width = 100; // CheckinTime
                    dgvCheckIn.Columns[5].Width = 120; // CheckoutDate
                    dgvCheckIn.Columns[6].Width = 100; // CheckoutTime
                    dgvCheckIn.Columns[7].Width = 100; // CarType
                    dgvCheckIn.Columns[8].Width = 100; // DriverType
                    //dgvCheckIn.Columns[9].Width = 120; // SRTCode

                }
            }


        }
        private void FrmParkingHistory_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);   
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
        }
    }
}
