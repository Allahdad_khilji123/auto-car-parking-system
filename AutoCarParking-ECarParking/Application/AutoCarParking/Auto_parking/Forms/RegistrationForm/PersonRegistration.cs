﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Auto_parking.Forms.RegistrationForm
{
    public partial class PersonRegistration : Form
    {
        public PersonRegistration()
        {
            InitializeComponent();
        }

        public PersonRegistration(string noflat)
        {
            InitializeComponent();
            txtRegNo.Text = noflat;
        }

        private void FillGrid(string searchvalue)
        {
            string query = string.Empty;
            if (string.IsNullOrEmpty(searchvalue))
            {
                query = "select PersonID [ID], FullName [Full Name], CNIC [CNIC], CellNo [Contact No], CarTypeID, CarType [Auto Type], DriverTypeID, DriverType [Driver Type], CarNo [Reg Flat No], RegDate [Reg Date], [Description] from v_PersonList ";
            }
            else
            {
                query = "select PersonID [ID], FullName [Full Name], CNIC [CNIC], CellNo [Contact No], CarTypeID, CarType [Auto Type], DriverTypeID, DriverType [Driver Type], CarNo [Reg Flat No], RegDate [Reg Date], [Description] from v_PersonList " +
                        "Where (FullName+' '+CNIC+' '+CellNo +' '+CarType +' '+DriverType+' '+ CarNo + ' ' +[Description]) like '%" + searchvalue.Trim() + "%'";
            }

            DataTable dt = DatabaseAccess.Select(query);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgvPersonList.DataSource = dt;
                    dgvPersonList.Columns[0].Width = 100; // PersonID
                    dgvPersonList.Columns[1].Width = 200; // FullName
                    dgvPersonList.Columns[2].Width = 120; // CNIC
                    dgvPersonList.Columns[3].Width = 120; // CellNo
                    dgvPersonList.Columns[4].Visible = false; // CarTypeID
                    dgvPersonList.Columns[5].Width = 120; // CarType
                    dgvPersonList.Columns[6].Visible = false; // DriverTypeID
                    dgvPersonList.Columns[7].Width = 150; // DriverType
                    dgvPersonList.Columns[8].Width = 120; // CarNo
                    dgvPersonList.Columns[9].Width = 130; // RegDate
                    dgvPersonList.Columns[10].Width = 300; // Description
                }
            }
        }

        private void ClearForm()
        {
            SourceCode.CombList.FillAutoTypes(cmbAutoType);
            SourceCode.CombList.FillDriverTypes(cmbDriverTypes);
            cmbAutoType.SelectedIndex = 0;
            cmbDriverTypes.SelectedIndex = 0;
            txtFullName.Clear();
            txtCNIC.Clear();
            txtContactNo.Clear();
            txtRegNo.Clear();
            txtDescription.Clear();
            txtSearch.Clear();
        }

        private void EnableControls()
        {
            btnSave.Enabled = false;
            txtSearch.Enabled = false;
            dgvPersonList.Enabled = false;
            btnCancel.Enabled = true;
            btnUpdate.Enabled = true;
        }

        private void DisableControls()
        {
            btnSave.Enabled = true;
            txtSearch.Enabled = true;
            dgvPersonList.Enabled = true;
            btnCancel.Enabled = false;
            btnUpdate.Enabled = false;
            ClearForm();
            FillGrid(string.Empty);
        }


        private void PersonRegistration_Load(object sender, EventArgs e)
        {
            FillGrid(string.Empty);
            SourceCode.CombList.FillAutoTypes(cmbAutoType);
            SourceCode.CombList.FillDriverTypes(cmbDriverTypes);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DisableControls();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            ep.Clear();   
            if (cmbAutoType.SelectedIndex == 0)
            {
                ep.SetError(cmbAutoType, "Please Select Auto Type!");
                cmbAutoType.Focus();
                return;
            }

            if (cmbDriverTypes.SelectedIndex == 0)
            {
                ep.SetError(cmbDriverTypes, "Required Field!");
                cmbDriverTypes.Focus();
                return;
            }

            if (txtFullName.Text.Trim().Length == 0)
            {
                ep.SetError(txtFullName, "Required Field!");
                txtFullName.Focus();
                return;
            }

            if (txtCNIC.Text.Trim().Length < 15)
            {
                ep.SetError(txtCNIC, "Required Field!");
                txtCNIC.Focus();
                return;
            }

            if (txtContactNo.Text.Trim().Length < 12)
            {
                ep.SetError(txtContactNo, "Required Field!");
                txtContactNo.Focus();
                return;
            }

            if (txtRegNo.Text.Trim().Length == 0)
            {
                ep.SetError(txtRegNo, "Required Field!");
                txtRegNo.Focus();
                return;
            }


            DataTable dt = DatabaseAccess.Select("select * from PersonTable where CarNo = '" + txtRegNo.Text.Trim() + "'");
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ep.SetError(txtRegNo, "Already Registered!");
                    txtRegNo.Focus();
                    txtRegNo.SelectAll();
                    return;
                }
            }


            string insertquery = string.Format("insert into PersonTable(DriverTypeID,CarTypeID,FullName,CNIC,CellNo,CarNo,RegDate,Description) values " +
                 " ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                 cmbDriverTypes.SelectedValue, cmbAutoType.SelectedValue, txtFullName.Text.Trim(),
                 txtCNIC.Text.Trim(), txtContactNo.Text.Trim(), txtRegNo.Text.Trim(),
                 DateTime.Now.ToString("yyyy/MM/dd"), txtDescription.Text.Trim());

            bool result = DatabaseAccess.InsertUpdateDelete(insertquery);
            if (result == true)
            {
                MessageBox.Show("Insert Successfully!");
                ClearForm();
                FillGrid(string.Empty);
            }
            else
            {
                MessageBox.Show("Some Error is Occur, please Try Again!");
            }

        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            ep.Clear();
            if (cmbAutoType.SelectedIndex == 0)
            {
                ep.SetError(cmbAutoType, "Please Select Auto Type!");
                cmbAutoType.Focus();
                return;
            }

            if (cmbDriverTypes.SelectedIndex == 0)
            {
                ep.SetError(cmbDriverTypes, "Required Field!");
                cmbDriverTypes.Focus();
                return;
            }

            if (txtFullName.Text.Trim().Length == 0)
            {
                ep.SetError(txtFullName, "Required Field!");
                txtFullName.Focus();
                return;
            }

            if (txtCNIC.Text.Trim().Length < 15)
            {
                ep.SetError(txtCNIC, "Required Field!");
                txtCNIC.Focus();
                return;
            }

            if (txtContactNo.Text.Trim().Length < 12)
            {
                ep.SetError(txtContactNo, "Required Field!");
                txtContactNo.Focus();
                return;
            }

            if (txtRegNo.Text.Trim().Length == 0)
            {
                ep.SetError(txtRegNo, "Required Field!");
                txtRegNo.Focus();
                return;
            }


            DataTable dt = DatabaseAccess.Select("select * from PersonTable where CarNo = '" + txtRegNo.Text.Trim() + "' AND PersonID != '" + Convert.ToString(dgvPersonList.CurrentRow.Cells[0].Value) + "'");
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    ep.SetError(txtRegNo, "Already Registered!");
                    txtRegNo.Focus();
                    txtRegNo.SelectAll();
                    return;
                }
            }


            string updatequery = string.Format("update PersonTable set DriverTypeID  = '{0}',CarTypeID = '{1}',FullName = '{2}',CNIC = '{3}',CellNo = '{4}',CarNo = '{5}',RegDate = '{6}',Description = '{7}' " +
                 " where PersonID = '{8}'",
                 cmbDriverTypes.SelectedValue, cmbAutoType.SelectedValue, txtFullName.Text.Trim(),
                 txtCNIC.Text.Trim(), txtContactNo.Text.Trim(), txtRegNo.Text.Trim(),
                 DateTime.Now.ToString("yyyy/MM/dd"), txtDescription.Text.Trim(),
                 Convert.ToString(dgvPersonList.CurrentRow.Cells[0].Value));

            bool result = DatabaseAccess.InsertUpdateDelete(updatequery);
            if (result == true)
            {
                MessageBox.Show("Update Successfully!");
                DisableControls();
            }
            else
            {
                MessageBox.Show("Some Error is Occur, please Try Again!");
            }
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvPersonList != null)
            {
                if (dgvPersonList.Rows.Count > 0)
                {
                    if (dgvPersonList.SelectedRows.Count == 1)
                    {
                        txtFullName.Text = Convert.ToString(dgvPersonList.CurrentRow.Cells[1].Value); // FullName
                        txtCNIC.Text = Convert.ToString(dgvPersonList.CurrentRow.Cells[2].Value); // CNIC
                        txtContactNo.Text = Convert.ToString(dgvPersonList.CurrentRow.Cells[3].Value); // CellNo
                        cmbAutoType.SelectedIndex = Convert.ToInt32(dgvPersonList.CurrentRow.Cells[4].Value); // CarTypeID
                        cmbDriverTypes.SelectedIndex = Convert.ToInt32(dgvPersonList.CurrentRow.Cells[6].Value); // DriverTypeID
                        txtRegNo.Text = Convert.ToString(dgvPersonList.CurrentRow.Cells[8].Value); // CarNo
                        txtDescription.Text = Convert.ToString(dgvPersonList.CurrentRow.Cells[10].Value); // Description
                        EnableControls();
                    }
                    else
                    {
                        MessageBox.Show("Please Select One Record!");
                    }
                }
            }
        }
    }
}
