USE [master]
GO
/****** Object:  Database [Auto_Car_ParkingDb]    Script Date: 02/01/2022 6:29:49 PM ******/
CREATE DATABASE [Auto_Car_ParkingDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Auto_Car_ParkingDb', FILENAME = N'D:\FYP Auto Car Parking Allahdad Nov 2021\Database\Auto_Car_ParkingDb.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Auto_Car_ParkingDb_log', FILENAME = N'D:\FYP Auto Car Parking Allahdad Nov 2021\Database\Auto_Car_ParkingDb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Auto_Car_ParkingDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET  MULTI_USER 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Auto_Car_ParkingDb]
GO
/****** Object:  Table [dbo].[CarTypeTable]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CarTypeTable](
	[CarTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CarType] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](300) NULL,
 CONSTRAINT [PK_CarTypeTable] PRIMARY KEY CLUSTERED 
(
	[CarTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DriverTypeTable]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriverTypeTable](
	[DriverTypeID] [int] IDENTITY(1,1) NOT NULL,
	[DriverType] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](300) NULL,
 CONSTRAINT [PK_DriverTypeTable] PRIMARY KEY CLUSTERED 
(
	[DriverTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ParkingTable]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkingTable](
	[ParkingID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[CheckinDate] [date] NOT NULL,
	[CheckinTime] [time](7) NOT NULL,
	[CheckoutDate] [date] NULL,
	[CheckoutTime] [time](7) NULL,
	[SRTCode] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonTable]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonTable](
	[PersonID] [int] IDENTITY(1,1) NOT NULL,
	[DriverTypeID] [int] NOT NULL,
	[CarTypeID] [int] NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[CNIC] [varchar](30) NOT NULL,
	[CellNo] [varchar](25) NOT NULL,
	[CarNo] [varchar](50) NOT NULL,
	[RegDate] [date] NOT NULL,
	[Description] [nvarchar](300) NULL,
 CONSTRAINT [PK_PersonTable] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTable](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[CellNo] [varchar](50) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_UserTable_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_PersonList]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_PersonList]
AS
SELECT        dbo.PersonTable.PersonID, dbo.PersonTable.FullName, dbo.PersonTable.CNIC, dbo.PersonTable.CellNo, dbo.PersonTable.CarTypeID, dbo.CarTypeTable.CarType, dbo.PersonTable.DriverTypeID, 
                         dbo.DriverTypeTable.DriverType, dbo.PersonTable.CarNo, dbo.PersonTable.RegDate, dbo.PersonTable.Description
FROM            dbo.PersonTable INNER JOIN
                         dbo.CarTypeTable ON dbo.PersonTable.CarTypeID = dbo.CarTypeTable.CarTypeID INNER JOIN
                         dbo.DriverTypeTable ON dbo.PersonTable.DriverTypeID = dbo.DriverTypeTable.DriverTypeID

GO
/****** Object:  View [dbo].[v_ParkingList]    Script Date: 02/01/2022 6:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_ParkingList]
AS
SELECT        dbo.ParkingTable.ParkingID, dbo.ParkingTable.PersonID, dbo.v_PersonList.FullName, dbo.v_PersonList.CarType, dbo.v_PersonList.DriverType, dbo.v_PersonList.CarNo, dbo.ParkingTable.CheckinDate, 
                         dbo.ParkingTable.CheckinTime, dbo.ParkingTable.SRTCode, dbo.ParkingTable.CheckoutDate, dbo.ParkingTable.CheckoutTime
FROM            dbo.ParkingTable INNER JOIN
                         dbo.v_PersonList ON dbo.ParkingTable.PersonID = dbo.v_PersonList.PersonID

GO
SET IDENTITY_INSERT [dbo].[CarTypeTable] ON 

INSERT [dbo].[CarTypeTable] ([CarTypeID], [CarType], [Description]) VALUES (1, N'Motor Car', N'asdfasdf')
INSERT [dbo].[CarTypeTable] ([CarTypeID], [CarType], [Description]) VALUES (2, N'Motor Bike', N'')
SET IDENTITY_INSERT [dbo].[CarTypeTable] OFF
SET IDENTITY_INSERT [dbo].[DriverTypeTable] ON 

INSERT [dbo].[DriverTypeTable] ([DriverTypeID], [DriverType], [Description]) VALUES (1, N'Students', N'')
INSERT [dbo].[DriverTypeTable] ([DriverTypeID], [DriverType], [Description]) VALUES (2, N'Staff', N'asdfasdf')
INSERT [dbo].[DriverTypeTable] ([DriverTypeID], [DriverType], [Description]) VALUES (3, N'Visitor', N'')
SET IDENTITY_INSERT [dbo].[DriverTypeTable] OFF
SET IDENTITY_INSERT [dbo].[PersonTable] ON 

INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (1, 1, 2, N'Asad', N'32165-4654654-6', N'0313-5756481', N'FF-4567', CAST(N'2021-10-20' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (2, 2, 1, N'Adbul Ghafor', N'98798-7984616-1', N'0313-5756481', N'KC-7867', CAST(N'2021-10-09' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (3, 1, 1, N'Hamza', N'17101-4545452-5', N'0313-5756481', N'88H8888', CAST(N'2021-11-30' AS Date), N'88H8888')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (4, 2, 2, N'Zia-ur-ehman', N'98798-6416531-3', N'0313-5756481', N'30Y9999', CAST(N'2021-11-30' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (5, 3, 2, N'Wali', N'65465-4654654-6', N'0313-5756481', N'30S9', CAST(N'2021-11-30' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (6, 1, 1, N'Mehran', N'65465-4646546-5', N'0313-5756481', N'29A15390', CAST(N'2021-11-30' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (7, 2, 1, N'XYZ', N'89078-9674653-4', N'0314-3076781', N'2505518', CAST(N'2021-11-30' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (8, 3, 1, N'Yasir', N'75645-3123123-1', N'0314-3076781', N'29A9580', CAST(N'2021-11-30' AS Date), N'')
INSERT [dbo].[PersonTable] ([PersonID], [DriverTypeID], [CarTypeID], [FullName], [CNIC], [CellNo], [CarNo], [RegDate], [Description]) VALUES (9, 2, 1, N'Gull ahmad', N'98794-6513213-2', N'0314-3076781', N'99234', CAST(N'2021-11-30' AS Date), N'')
SET IDENTITY_INSERT [dbo].[PersonTable] OFF
SET IDENTITY_INSERT [dbo].[UserTable] ON 

INSERT [dbo].[UserTable] ([UserID], [UserName], [Password], [FullName], [CellNo], [Address], [Description], [IsActive]) VALUES (1, N'Admin', N'admin', N'Salman Khan', N'03145656568', N'abc', N'ABC', 1)
SET IDENTITY_INSERT [dbo].[UserTable] OFF
ALTER TABLE [dbo].[ParkingTable]  WITH CHECK ADD  CONSTRAINT [FK_ParkingTable_PersonTable] FOREIGN KEY([PersonID])
REFERENCES [dbo].[PersonTable] ([PersonID])
GO
ALTER TABLE [dbo].[ParkingTable] CHECK CONSTRAINT [FK_ParkingTable_PersonTable]
GO
ALTER TABLE [dbo].[PersonTable]  WITH CHECK ADD  CONSTRAINT [FK_PersonTable_CarTypeTable] FOREIGN KEY([CarTypeID])
REFERENCES [dbo].[CarTypeTable] ([CarTypeID])
GO
ALTER TABLE [dbo].[PersonTable] CHECK CONSTRAINT [FK_PersonTable_CarTypeTable]
GO
ALTER TABLE [dbo].[PersonTable]  WITH CHECK ADD  CONSTRAINT [FK_PersonTable_DriverTypeTable] FOREIGN KEY([DriverTypeID])
REFERENCES [dbo].[DriverTypeTable] ([DriverTypeID])
GO
ALTER TABLE [dbo].[PersonTable] CHECK CONSTRAINT [FK_PersonTable_DriverTypeTable]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ParkingTable"
            Begin Extent = 
               Top = 6
               Left = 30
               Bottom = 200
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v_PersonList"
            Begin Extent = 
               Top = 6
               Left = 238
               Bottom = 211
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ParkingList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_ParkingList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PersonTable"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "CarTypeTable"
            Begin Extent = 
               Top = 100
               Left = 258
               Bottom = 213
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DriverTypeTable"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 119
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_PersonList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_PersonList'
GO
USE [master]
GO
ALTER DATABASE [Auto_Car_ParkingDb] SET  READ_WRITE 
GO
